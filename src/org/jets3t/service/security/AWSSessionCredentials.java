package org.jets3t.service.security;

import org.jets3t.service.Jets3tProperties;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClient;
import com.amazonaws.services.securitytoken.model.AssumeRoleRequest;
import com.amazonaws.services.securitytoken.model.AssumeRoleResult;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;

import java.util.Calendar;
import java.util.Date;


/**
 * Class to contain the temporary (session-based) Amazon Web Services (AWS) credentials of a user.
 *
 */
public class AWSSessionCredentials extends ProviderCredentials {

  protected String iamAccessKey = null;
  protected String iamSecretKey = null;
  protected boolean isClusterOnRole = false;
  protected String primaryRoleToBeAssumed = null;
  protected String primaryExternalId = null;
  protected String secondaryRoleToBeAssumed = null;
  protected String secondaryExternalId = null;

  protected String sessionToken = null;
  protected Date expirationDate = null;

  protected Jets3tProperties jets3tProperties;
  private static final String DEFAULT_SESSION_NAME = "some-session-name";

  private static InstanceProfileCredentialsProvider INSTANCE_PROFILE_CREDENTIALS_PROVIDER = new InstanceProfileCredentialsProvider();

  public AWSSessionCredentials(String iamAccessKey, String iamSecretKey, boolean isClusterOnRole,
                               String primaryRoleToBeAssumed, String primaryExternalId,
                               String secondaryRoleToBeAssumed, String secondaryExternalId,
                               String friendlyName, Jets3tProperties jets3tProperties){
    if(primaryRoleToBeAssumed == null || "".equals(primaryRoleToBeAssumed)){
      throw new IllegalArgumentException("roleToBeAssumed needs to be present.");
    }
    this.iamAccessKey = iamAccessKey;
    this.iamSecretKey = iamSecretKey;
    this.isClusterOnRole = isClusterOnRole;
    this.primaryRoleToBeAssumed = primaryRoleToBeAssumed;
    this.primaryExternalId = primaryExternalId;
    if(!primaryRoleToBeAssumed.equals(secondaryRoleToBeAssumed)){
      this.secondaryRoleToBeAssumed = secondaryRoleToBeAssumed;
      this.secondaryExternalId = secondaryExternalId;
    }
    this.friendlyName = friendlyName;
    this.jets3tProperties = jets3tProperties;
    assumeRoleAndGetCredentials();
  }

  /**
   * Gets a set of new credentials.
   * Step 1. : If isClusterOnRole is true, it takes the role attached on the Ec2 instance, else it takes the access-keys
   * Step 2. : Assumes the primaryRole
   * Step 3. : Assumes the secondaryRole if it is not null
   * Finally, it returns the temporary credentials, got in Step 2. ( if secondaryRole is null ) or Step 3. ( if secondaryRole is not null )
   */
  private void assumeRoleAndGetCredentials() {
    AWSSecurityTokenServiceClient stsClient = null;
    if(this.isClusterOnRole){
      com.amazonaws.auth.AWSSessionCredentials awsSessionCredentials = (com.amazonaws.auth.AWSSessionCredentials) INSTANCE_PROFILE_CREDENTIALS_PROVIDER.getCredentials();
      stsClient =
              new AWSSecurityTokenServiceClient(awsSessionCredentials);
    }else {
      com.amazonaws.auth.AWSCredentials awsCredentials = new BasicAWSCredentials(iamAccessKey, iamSecretKey);
      stsClient =
              new AWSSecurityTokenServiceClient(awsCredentials);
    }

    AssumeRoleResult assumeRoleResult = getAssumeRoleResult(stsClient, primaryRoleToBeAssumed, primaryExternalId);

    if(secondaryRoleToBeAssumed != null && !"".equals(secondaryRoleToBeAssumed)) {
      com.amazonaws.auth.BasicSessionCredentials primaryRoleSessionCredentials =
              new com.amazonaws.auth.BasicSessionCredentials(assumeRoleResult.getCredentials().getAccessKeyId(),
                      assumeRoleResult.getCredentials().getSecretAccessKey(), assumeRoleResult.getCredentials().getSessionToken());
      stsClient =
              new AWSSecurityTokenServiceClient(primaryRoleSessionCredentials);
      assumeRoleResult = getAssumeRoleResult(stsClient, secondaryRoleToBeAssumed, secondaryExternalId);
    }
    this.accessKey = assumeRoleResult.getCredentials().getAccessKeyId();
    this.secretKey = assumeRoleResult.getCredentials().getSecretAccessKey();
    this.sessionToken = assumeRoleResult.getCredentials().getSessionToken();
    this.expirationDate = assumeRoleResult.getCredentials().getExpiration();
  }

  private AssumeRoleResult getAssumeRoleResult(AWSSecurityTokenServiceClient stsClient, String roleToBeAssumed, String externalId) {
    int defaultRequestedExpiryTimeInMinutes = jets3tProperties.getIntProperty("aws.session-credentials.expiry-time.to-be-requested", 60);
    AssumeRoleRequest assumeRequest = new AssumeRoleRequest()
            .withRoleArn(roleToBeAssumed)
             .withDurationSeconds(defaultRequestedExpiryTimeInMinutes * 60)
            .withRoleSessionName(DEFAULT_SESSION_NAME);
    if(externalId != null) {
      assumeRequest = assumeRequest.withExternalId(externalId);
    }
    return stsClient.assumeRole(assumeRequest);
  }

  private boolean isExpired(){
    Calendar instance = Calendar.getInstance();
    // TODO : this is being done to avoid race conditions, need to explore a better way
    instance.add(Calendar.MINUTE, 5);
    Date now = instance.getTime();
    if (expirationDate.compareTo(now) < 0) {
      return true;
    }
    return false;
  }

  @Override
  protected String getTypeName() {
    return "session";
  }

  @Override
  protected String getVersionPrefix() { return "jets3t AWS Credentials, version: "; }

  /**
   * @return
   * The AWS session token for temporary/session-based account credentials.
   */
  @Override
  public String getSessionToken() {
    return this.sessionToken;
  }

  @Override
  public synchronized ProviderCredentials refreshAndGetCredentials() {
    if(isExpired() == true) {
      assumeRoleAndGetCredentials();
    }
    return new AWSCredentials(accessKey, secretKey){

      @Override
      public String getSessionToken() {
        return sessionToken;
      }

    };
  }

}

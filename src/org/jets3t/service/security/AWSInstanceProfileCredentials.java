package org.jets3t.service.security;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;

public class AWSInstanceProfileCredentials extends ProviderCredentials {

  private static InstanceProfileCredentialsProvider INSTANCE_PROFILE_CREDENTIALS_PROVIDER = new InstanceProfileCredentialsProvider();
  protected String sessionToken = null;

  public AWSInstanceProfileCredentials(String friendlyName) {
    this.friendlyName = friendlyName;
  }

  @Override
  protected String getTypeName() {
    return "instance-credentials";
  }

  @Override
  public String getVersionPrefix() {
    return "jets3t AWS Credentials, version: ";
  }

  @Override
  public String getSessionToken() {
    return this.sessionToken;
  }

  @Override
  public synchronized ProviderCredentials refreshAndGetCredentials() {
    com.amazonaws.auth.AWSSessionCredentials awsSessionCredentials = (com.amazonaws.auth.AWSSessionCredentials) INSTANCE_PROFILE_CREDENTIALS_PROVIDER.getCredentials();
    this.accessKey = awsSessionCredentials.getAWSAccessKeyId();
    this.secretKey = awsSessionCredentials.getAWSSecretKey();
    this.sessionToken = awsSessionCredentials.getSessionToken();

    return new AWSCredentials(accessKey, secretKey){

      @Override
      public String getSessionToken() {
        return sessionToken;
      }

    };
  }

}
